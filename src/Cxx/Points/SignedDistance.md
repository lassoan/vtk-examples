### Description
Contrast this with the [UnsignedDistance](UnsignedDistance) example.

The image was created using the [Armadillo dataset](https://github.com/lorensen/VTKExamples/blob/master/src/Testing/Data/Armadillo.ply?raw=true)
